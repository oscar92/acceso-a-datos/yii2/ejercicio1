<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Lleva;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    //DAO
    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('select distinct edad from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['edad'],
        "titulo"=>"consulta 1 con DAO",
        "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
        "sql"=>"SELECT DISTINCT edad FROM ciclista",
    ]);
    }
    
    public function actionConsulta2(){
        
        $numero = Yii::$app->db
                ->createCommand('select edad from ciclista where nomequipo = "Artiach"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT edad FROM ciclista WHERE nomequipo = "Artiach"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['edad'],
        "titulo"=>"consulta 2 con DAO",
        "enunciado"=>"listar las edades de los ciclistas de Artiach",
        "sql"=>'SELECT edad FROM ciclista WHERE nomequipo = "Artiach"',
    ]);
    }
    
    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand('select edad from ciclista where nomequipo = "Artiach" or nomequipo = "Amore Vitae"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vitae"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['edad'],
        "titulo"=>"consulta 3 con DAO",
        "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",
        "sql"=>'SELECT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vitae"',
    ]);
    }
    
    public function actionConsulta4(){
        
        $numero = Yii::$app->db
                ->createCommand('select dorsal from ciclista where edad <25 or edad >30')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM ciclista WHERE edad <25 OR edad >30',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"consulta 4 con DAO",
        "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
        "sql"=>'SELECT dorsal FROM ciclista WHERE edad <25 OR edad >30',
    ]);
    }
    
    public function actionConsulta5(){
        
        $numero = Yii::$app->db
                ->createCommand('select dorsal  from ciclista where edad >28 and edad <32 and nomequipo = "Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal  FROM ciclista WHERE edad >28 AND edad <32 AND nomequipo = "Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"consulta 5 con DAO",
        "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
        "sql"=>'SELECT dorsal  FROM ciclista WHERE edad >28 AND edad <32 AND nomequipo = "Banesto"',
    ]);
    }
    
    public function actionConsulta6(){
        
        $numero = Yii::$app->db
                ->createCommand('select nombre from ciclista where length(nombre) >8')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre FROM ciclista WHERE LENGTH(nombre) >8',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['nombre'],
        "titulo"=>"consulta 6 con DAO",
        "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
        "sql"=>'SELECT nombre FROM ciclista WHERE LENGTH(nombre) >8',
    ]);
    }
    
    public function actionConsulta7(){
        
        $numero = Yii::$app->db
                ->createCommand('select upper(nombre) as mayusculas, dorsal from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT UPPER(nombre) AS mayusculas, dorsal FROM ciclista',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['mayusculas','dorsal'],
        "titulo"=>"consulta 7 con DAO",
        "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
        "sql"=>'SELECT UPPER(nombre) AS mayusculas, dorsal FROM ciclista',
    ]);
    }
    
    public function actionConsulta8(){
        
        $numero = Yii::$app->db
                ->createCommand('select dorsal from lleva where código="MGE"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM lleva WHERE código="MGE"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"consulta 8 con DAO",
        "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
        "sql"=>'SELECT dorsal FROM lleva WHERE código="MGE"',
    ]);
    }
    
    public function actionConsulta9(){
        
        $numero = Yii::$app->db
                ->createCommand('select nompuerto from puerto where altura > 1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nompuerto FROM puerto WHERE altura > 1500',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['nompuerto'],
        "titulo"=>"consulta 9 con DAO",
        "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500 ",
        "sql"=>'SELECT nompuerto FROM puerto WHERE altura > 1500',
    ]);
    }
    
    public function actionConsulta10(){
        
        $numero = Yii::$app->db
                ->createCommand('select dorsal from puerto where pendiente >8 or (altura>1800 and altura<3000)')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM puerto WHERE pendiente >8 OR (altura>1800 AND altura<3000)',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"consulta 10 con DAO",
        "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000 ",
        "sql"=>'SELECT dorsal FROM puerto WHERE pendiente >8 OR (altura>1800 AND altura<3000)',
    ]);
    }
    
    public function actionConsulta11(){
        
        $numero = Yii::$app->db
                ->createCommand('select dorsal from puerto where pendiente >8 and (altura>1800 and altura<3000)')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM puerto WHERE pendiente >8 AND (altura>1800 AND altura<3000)',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"consulta 11 con DAO",
        "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
        "sql"=>'SELECT dorsal FROM puerto WHERE pendiente >8 AND (altura>1800 AND altura<3000)',
    ]);
    }
    
    //Active Record
    public function actionConsultala1(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsultala2(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"listar las edades de los ciclistas de Artiach",
            "sql"=>'SELECT edad FROM ciclista WHERE nomequipo = "Artiach"',
        ]);
    }
    
    public function actionConsultala3(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("nomequipo='Artiach' OR nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>'SELECT edad FROM ciclista WHERE nomequipo = "Artiach" or nomequipo = "Amore Vita"',
        ]);
    }
    
    public function actionConsultala4(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->where("edad <25 OR edad >30"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>'SELECT dorsal  FROM ciclista c WHERE edad <25 OR c.edad >30',
        ]);
    }
    
    public function actionConsultala5(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->where("edad >28 AND edad <32 AND nomequipo = 'Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>'SELECT dorsal  FROM ciclista WHERE edad >28 AND edad <32 AND nomequipo = "Banesto"',
        ]);
    }
    
    public function actionConsultala6(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre")->where("LENGTH(nombre) >8"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>'SELECT nombre FROM ciclista WHERE LENGTH(nombre) >8',
        ]);
    }
    
    public function actionConsultala7(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("UPPER(nombre) AS mayusculas, dorsal"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['mayusculas','dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>" lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>'SELECT UPPER(nombre) AS mayusculas, dorsal FROM ciclista',
        ]);
    }
    
     public function actionConsultala8(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()->select("dorsal")->where("código='MGE'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>'SELECT dorsal FROM lleva WHERE código="MGE"',
        ]);
    }
    
    public function actionConsultala9(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto")->where("altura > 1500"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>'SELECT nompuerto FROM puerto WHERE altura > 1500',
        ]);
    }
    
    public function actionConsultala10(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->where("pendiente >8 OR (altura>1800 AND altura<3000)"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>'SELECT dorsal FROM puerto WHERE pendiente >8 OR (altura>1800 AND altura<3000)',
        ]);
    }
    
    public function actionConsultala11(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->where("pendiente >8 AND (altura>1800 AND altura<3000)"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>'SELECT dorsal FROM puerto WHERE pendiente >8 AND (altura>1800 AND altura<3000)',
        ]);
    }
}
