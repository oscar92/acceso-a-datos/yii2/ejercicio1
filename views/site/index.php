<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección 1</h1>
        
    </div>

    <div class="body-content">

        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h2>Consulta 1</h2>

                <p>Listar las edades de los ciclistas (sin repetidos)</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala1'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
                
            </div>
            <div  class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h2>Consulta 2</h2>

                <p>Listar las edades de los ciclistas de Artiach</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala2'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
                
            </div>
            <div  class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h2>Consulta 3</h2>
                
                <p>Listar las edades de los cilcistas de Artiach o de<br>
                    Amore Vitae</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala3'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 4</h2>

                <p>Listar los dorsales de los ciclistas cuya edad sea<br>
                    menor que 25 o mayor que 30</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala4'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 5</h2>

                <p>Listar los dorsales de los ciclistas cuya edad este<br>
                    entre 28 y 32 y ademas solo sean de Banesto</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala5'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 6</h2>
                
                <p>Indicame el nombre de los ciclistas que el numero de<br>
                    caracteres del nombre sea mayor que 8</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala6'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 7</h2>

                <p>Listame el nombre y el dorsal de todos lor ciclistas <br>
                    mostrando un campo nuevo denominado nombre<br>
                    mayusculas que deba mostrar el nombre en<br>
                mayusculas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala7'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 8</h2>

                <p>Listar todos los ciclistas que han ganado el maillot<br>
                MGE (amarillo) en alguna etapa</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala8'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 9</h2>
                
                <p>Listar el nombre de los puertos cuya altura sea<br>
                mayor de 1500</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala9'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 10</h2>

                <p>Listar el dorsal de los ciclistas que hayan ganado<br>
                    algun puerto cuya pendiente sea mayor que 8 o cuya altura<br> 
                    esté entre 1800 y 3000 </p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala10'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 11</h2>

                <p>Listar el dorsal de los ciclistas que hayan ganado<br>
                    algún puerto cuya pendiente sea mayor que 8 y cuya altura<br>
                    esté entre 1800 y 3000</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala11'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
